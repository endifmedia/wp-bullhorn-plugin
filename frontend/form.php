<?php
//$bullhorn_form = get_option( 'bullhorn_gravity_form' );

/*$form = GFAPI::get_form($bullhorn_form);
echo '<div style="display:none"><pre>';
var_dump($form);
echo "</pre></div>";*/

if(isset($_POST['gform_submit'])){

/*echo '<style>
	.loader{
		display:block;
	}
</style>';*/
// Address data
$address1	= sanitize_text_field( $_POST['input_4_1'] );
$city		= sanitize_text_field( $_POST['input_4_3'] );
$state		= $_POST['input_4_4'];
//$zip		=$_POST['zip'];
$zip		= intval( $_POST['input_4_5'] );
$countryID	= 1;

//User details
//$namesufix    =	$_POST['namesufix'];
$firstName    =	sanitize_text_field( $_POST['input_2_3'] );
$lastName     =	sanitize_text_field( $_POST['input_2_6'] );
//$nameprefix   =	$_POST['nameprefix'];
$phone        =	sanitize_text_field( $_POST['input_6'] );
$email        =	sanitize_email( $_POST['input_3'] );
$address   	  =	sanitize_text_field( $_POST['input_4_5'] );
$description  =	sanitize_text_field( $_POST['input_5'] );
//$mobile       =	$_POST['mobile'];
//$fax          =	$_POST['fax'];

$params = array(
	'trace' => 1,
	'soap_version' => SOAP_1_1);
$BHClient = new SoapClient("https://api.bullhornstaffing.com/webservices-1.1/?wsdl",$params);

//get associated variables
$username = get_option('bullhorn_username');
$password = get_option('bullhorn_password');
$apiKey = get_option('bullhorn_api_key');

$session_request = new stdClass(); 
$session_request->username = $username; 
$session_request->password = $password;
$session_request->apiKey = $apiKey;
$API_session = $BHClient->startSession($session_request);
$API_currentSession = $API_session->return; 


// Creating a candidate
$address_array = array(
	'address1'  => $address1,
	'city'      => $city,
	'state'     => $state,
	'zip'       => $zip,
	'countryID' => $countryID
);

/* To make the username unique, use the first initial and last name with a random number. You can use any mechanism for generating the random number. */
	//$random_num = rand(1000,9999);
	//$username = $firstName.$random_num;
	$username = $firstName . ' ' . $lastName;

$candidate_array = array(
	'firstName' => $firstName,
	'lastName ' => $lastName,
	'name'	    => $username,
	'phone'     => $phone,
	'email'     => $email,
	'address'   => $address_array,
	'username'  => $username,
	'status'    => 'New Lead',
	'description' => $description,
	'ownerID'   => '2',
	'userTypeID' => 35,
	'categoryID' => 45,
	'password'   => 'password'
	//'mobile' 	 => $mobile,
	//'fax'		 => $fax
);
$SOAP_dto = new SoapVar($candidate_array, SOAP_ENC_OBJECT, "candidateDto", "http://candidate.entity.bullhorn.com/");

$request_array = array(
	'session' => $API_currentSession,
	'dto' => $SOAP_dto
);

$SOAP_request = new SoapVar($request_array, SOAP_ENC_OBJECT, "save", "http://save.apiservice.bullhorn.com/");

try{
	$saveResponse = $BHClient->save($SOAP_request);
} catch (SoapFault $fault) {
	var_dump($BHClient->__getLastRequest());
	die($fault->faultstring);
}

$API_currentSession = $saveResponse->return->session;
$savedCandidate = $saveResponse->return->dto;


$success=$savedCandidate->userID;

if($success==''){
	$message='Your application was successsful!';
}else{

}

/*echo '<style>
	.loader{
		display:none;
	}
</style>';*/

}
?>

<div id="apply-now" style="width:100%;">
	<div id="gform_wrapper_4" class="gf_browser_gecko gform_wrapper">
		<?php 
			echo do_shortcode('[gravityform id="' . get_option( 'bullhorn_gravity_form' ) . '" name="" title="false" description="false"]');
		?>
	<!---
		<form action="" id="gform_4" enctype="multipart/form-data" method="post">
			<div class="gform_heading">
				<h3 class="gform_title">Apply Now</h3>
				<span class="gform_description">If you are interested in this position, we welcome you to begin the application process online. Please complete this short form, paste in your cover letter, and attach your resume.</span>
			</div>
			<div class="form">
				<div class="row">
					<div class="label">
						Name <span class="red">*</span>
					</div>
					<div class="field">
						<div class="ss">
							<input type="text" placeholder="prefix" required name="namesufix">
						</div>
						<div class="sss">
							<input type="text" placeholder="firstname" required name="firstName">
						</div>
						<div class="sss">
							<input type="text" placeholder="lastname" required name="lastName">
						</div>
						<div class="ss">
							<input type="text" placeholder="suffix" required name="nameprefix">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="label">
						Email <span class="red">*</span>
					</div>
					<div class="field">
						<input type="email" placeholder="abc@abc.com" required name="email">
					</div>
				</div>
				<div class="row1">
					<span>Address <span class="red">*</span></span>
					<div class="label">
						Street Address <span class="red">*</span>
					</div>
					<div class="field">
						<input type="text" placeholder="abc" required name="address">
					</div>
				</div>
				<div class="row1">
					<div class="label">
						Address Line 2 <span class="red">*</span>
					</div>
					<div class="field">
						<input type="text" placeholder="abc" required name="address1">
					</div>
				</div>
				<div class="row2">
					<div class="label">
						City <span class="red">*</span>
					</div>
					<div class="field">
						<input type="text" placeholder="abc" required name="city">
					</div>
				</div>
				<div class="row2">	
					<div class="label">
						State <span class="red">*</span>
					</div>
					<div class="field">
						<select required name="state">
							<option selected="selected" value="state"></option>
							<option value="Alabama">Alabama</option>
							<option value="Alaska">Alaska</option>
							<option value="Arizona">Arizona</option>
							<option value="Arkansas">Arkansas</option>
							<option value="California">California</option>
							<option value="Colorado">Colorado</option>
							<option value="Connecticut">Connecticut</option>
							<option value="Delaware">Delaware</option>
							<option value="District of Columbia">District of Columbia</option>
							<option value="Florida">Florida</option>
							<option value="Georgia">Georgia</option>
							<option value="Hawaii">Hawaii</option>
							<option value="Idaho">Idaho</option>
							<option value="Illinois">Illinois</option>
							<option value="Indiana">Indiana</option>
							<option value="Iowa">Iowa</option>
							<option value="Kansas">Kansas</option>
							<option value="Kentucky">Kentucky</option>
							<option value="Louisiana">Louisiana</option>
							<option value="Maine">Maine</option>
							<option value="Maryland">Maryland</option>
							<option value="Massachusetts">Massachusetts</option>
							<option value="Michigan">Michigan</option>
							<option value="Minnesota">Minnesota</option>
							<option value="Mississippi">Mississippi</option>
							<option value="Missouri">Missouri</option>
							<option value="Montana">Montana</option>
							<option value="Nebraska">Nebraska</option>
							<option value="Nevada">Nevada</option>
							<option value="New Hampshire">New Hampshire</option>
							<option value="New Jersey">New Jersey</option>
							<option value="New Mexico">New Mexico</option>
							<option value="New York">New York</option>
							<option value="North Carolina">North Carolina</option>
							<option value="North Dakota">North Dakota</option>
							<option value="Ohio">Ohio</option>
							<option value="Oklahoma">Oklahoma</option>
							<option value="Oregon">Oregon</option>
							<option value="Pennsylvania">Pennsylvania</option>
							<option value="Rhode Island">Rhode Island</option>
							<option value="South Carolina">South Carolina</option>
							<option value="South Dakota">South Dakota</option>
							<option value="Tennessee">Tennessee</option>
							<option value="Texas">Texas</option>
							<option value="Utah">Utah</option>
							<option value="Vermont">Vermont</option>
							<option value="Virginia">Virginia</option>
							<option value="Washington">Washington</option>
							<option value="West Virginia">West Virginia</option>
							<option value="Wisconsin">Wisconsin</option>
							<option value="Wyoming">Wyoming</option>
							<option value="Armed Forces Americas">Armed Forces Americas</option>
							<option value="Armed Forces Europe">Armed Forces Europe</option>
							<option value="Armed Forces Pacific">Armed Forces Pacific</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="label">
						Zip Code <span class="red">*</span>
					</div>
					<div class="field">
						<input type="text" placeholder="abc" required name="zip">
					</div>
				</div>
				<div class="row">
					<div class="label">
						Phone <span class="red">*</span>
					</div>
					<div class="field">
						<input type="text" placeholder="1111111111" required name="phone">
					</div>
				</div>
				<div class="row">
					<div class="label">
						Fax
					</div>
					<div class="field">
						<input type="text" placeholder="xx-xx-xxxx" name="fax">
					</div>
				</div>
				<div class="row">
					<div class="label">
						Mobile
					</div>
					<div class="field">
						<input type="text" placeholder="+65 11111 111111" name="mobile">
					</div>
				</div>
				<div class="row">
					<div class="label">
						Cover Letter <span class="red">*</span>
					</div>
					<div class="field">
						<?php
						//$content = '';
						//$editor_id = 'description';
						//wp_editor( $content, $editor_id );

						?>
					</div>
				</div>
				<!---<div class="row">
					<div class="label">
						Resume Upload <span class="red">*</span>
					</div>
					<div class="field">
						<input type="file" placeholder="abc@abc.com" required>
					</div>
				</div>--->
		<!---		<div class="row">
					<div class="field">
						<input type="submit" value="submit" id="submit" name="submit">
					</div>
				</div>
			</form>-->
		</div>
	</div>
</div>


