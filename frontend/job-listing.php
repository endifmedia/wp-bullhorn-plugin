<?php

		error_reporting(1);
		
		//Query for getting data from database
		$username = get_option('bullhorn_username');
		$password = get_option('bullhorn_password');
		$apiKey = get_option('bullhorn_api_key');
		$target = get_option('bullhorn_jobs');

		
		// If no api key , username ,password  is specified
		if(empty($username)){
			echo "Invalid Authentication. Please input username, password, and API key";
		}else{

		$params = array(
			'trace' => 1,
			'soap_version' => SOAP_1_1);
		$BHclient = new SoapClient("https://api.bullhornstaffing.com/webservices-1.1/?wsdl",$params);
		
		// To run this code, you will need a valid username, password, and API key.
		
		// Start a new session
		$session_request = new stdClass();
		$session_request->username = $username;
		$session_request->password = $password;
		$session_request->apiKey = $apiKey;
		$API_session = $BHclient->startSession($session_request);
		$API_currentSession = $API_session->return;
		
		// Create an array with the query parameters
		$query_array = array(
		'entityName' => 'JobOrder',
		//'maxResults' => '205',//$target,
		'where' => 'isDeleted=0',
		'parameters' => array()
		);
		// Create the DTO type that the API will understand by casting the array to the dtoQuery
		// type that the query operation expects. 
		$SOAP_query = new SoapVar($query_array, SOAP_ENC_OBJECT,"dtoQuery", "http://query.apiservice.bullhorn.com/");
		
		// Put the DTO into a request object
		$request_array = array (
		'session' => $API_currentSession,
		'query' => $SOAP_query);
		
		// Cast the request as a query type
		$SOAP_request = new SoapVar($request_array, SOAP_ENC_OBJECT, "query", "http://query.apiservice.bullhorn.com/");
		
		// Use the query method to return the Jobs ids
		try {
			$queryResult = $BHclient->query($SOAP_request);
		} 
		catch (SoapFault $fault) {
			var_dump($BHclient->__getLastRequest());
			die($fault->faultstring);
		}

		// Use the find() method to retrieve the Jobs DTO for each Id
		// Loop through each Id in the query result list
		if($_REQUEST['job_id']!='' ||$_REQUEST['job_id']!=null){
			$message='';
			$jobId=$_REQUEST['job_id'];
			// Cast each Id to an integer type
			$findId = new SoapVar($jobId, XSD_INTEGER,"int","http://www.w3.org/2001/XMLSchema");
			// Create the find() method request
			$find_request = array(
				'session' => $API_currentSession,
				'entityName' => 'JobOrder',
				'id' => $findId
			);
			// Use the find() method to return the Jobs dto
			try {
				$findResult = $BHclient->find($find_request);
			} 
			catch (SoapFault $fault) {
				var_dump($BHclient->__getLastRequest());
				die($fault->faultstring);
			}

			// Print fields from the dto object
		 	$postedTime=date('F d,Y',strtotime($findResult->return->dto->dateAdded));
			echo '<div class="job_area">';
			//echo '<div class="JobdateFront">Posted on <span>'.$postedTime.'</span> by '.$postedTime.'<a href="#apply-now">Apply Now</a></div>';
			echo $message;
			echo '<div class="JobdateFront">Posted on <span>'.$postedTime.'</span> <a href="javascript:void(0)" id="apply">Apply Now</a></div>';
			echo '<div class="left_job_area_front">';
			echo '<h1 class="title">'.$findResult->return->dto->title.' | <span> Job ID : '.$jobId.'</span></h1>';
			echo '</div>';
			echo '<div class="left_job_right_single">';
			echo '<p class="" style="margin:0">'.$findResult->return->dto->description.'</p>';
			echo '</div>';
			echo '</div>'; 
			include("form.php");

		} else {

			$loop = '1';
			$queryResult->return->ids;
			foreach ($queryResult->return->ids as $value){
				/*echo '<style>
						.loader{
							display:block !important;
						}
					</style>';*/
				// Cast each Id to an integer type
				$findId = new SoapVar($value, XSD_INTEGER,"int","http://www.w3.org/2001/XMLSchema");

				// Create the find() method request
				$find_request = array(
					'session' => $API_currentSession,
					'entityName' => 'JobOrder',
					'id' => $findId,
				);
				// Use the find() method to return the Jobs dto
				try {
					$findResult = $BHclient->find($find_request);
				} 
				catch (SoapFault $fault) {
					var_dump($BHclient->__getLastRequest());
					die($fault->faultstring);
				}			

				//create excerpt out of description
				$link_to_post = get_the_permalink() . '?job_id=' . $findResult->return->dto->jobOrderID;

				//check if post is LIVE before printing
				if($findResult->return->dto->isOpen == '1') {
					if($loop <= $target) {
						echo '<div class="job_area">';
							echo '<div class="left_job_area">';
								echo '<h1 class="title"><a href="' . get_the_permalink() . '?job_id='.$findResult->return->dto->jobOrderID . '">' .$findResult->return->dto->title.'</a></h1>';
							echo '</div>';
							echo '<div class="middle_job_area">';
							if($findResult->return->dto->employmentType){
								echo '<div class="Jobdate job-type">'.$findResult->return->dto->employmentType.'</div>';
							}
							if($findResult->return->dto->address->address1){
								echo '<div class="Jobdate job-address">'.$findResult->return->dto->address->address1.'</div>';
							}
							//if($findResult->return->dto->jobOrderID){
							//	echo '<div class="Jobdate job-id">Job ID : ' . $findResult-//>return->dto->jobOrderID . '</div>';
							//}
							echo '</div>';
							echo '<div class="left_job_right">';
								//echo '<p class="description">'.string_limit_words($findResult->return->dto->description, 20, $link_to_post).' <span><a style="float:right;" href=' .$_SERVER['REQUEST_URI'].'?job_id='.$value.'>  Read more</a></span></p>';
								echo '<p class="description">' . string_limit_words($findResult->return->dto->description, 20, $link_to_post) . '</p>';
							echo '</div>';
						echo '</div>'; 

						$loop++;
					}
			    }
			}
			/*echo '<style>
					.loader{
						display:none !important;
					}
				</style>';*/
		}
	}
	?>
<link rel="stylesheet" href="<?php echo plugins_url(); ?>/wp_bullhorn/css/frontend.css">
<script src="<?php echo plugins_url(); ?>/wp_bullhorn/js/jquery.js"></script>
<script>
	$(document).ready(function(){
		$('#apply').click(function(){
	 			$('html,body').animate({scrollTop: $("#apply-now").offset().top -30}, 2000)	; 						
		});
	});
</script>
<div class="loader" style="display:none;">
	<div id="floatingBarsG">
	<div class="blockG" id="rotateG_01">
	</div>
	<div class="blockG" id="rotateG_02">
	</div>
	<div class="blockG" id="rotateG_03">
	</div>
	<div class="blockG" id="rotateG_04">
	</div>
	<div class="blockG" id="rotateG_05">
	</div>
	<div class="blockG" id="rotateG_06">
	</div>
	<div class="blockG" id="rotateG_07">
	</div>
	<div class="blockG" id="rotateG_08">
	</div>
	</div>
</div>

