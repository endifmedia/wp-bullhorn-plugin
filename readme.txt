=== WP Bullhorn ===
Contributors: ENDif Media
Tags: Bullhorn, CRM, Relationship Management System
Requires at least: 4.0
Tested up to: 4.3.1
Stable tag: 1.1
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Fetches and displays job listings from your Bullhorn account. To use, just add [wp_bullhorn] shortcode to any page or post.


== Description ==

ENJOY!

== Installation ==
1. Simply download the plugin and install.
2. Add you Bullhorn API Key, username, and password.
3. Set how many job posts you want to display.
4. Save your options!

== Changelog ==

= 1.1 =
Initial Release