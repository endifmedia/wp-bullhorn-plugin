<?php
/*
Plugin Name: WP_BULLHORN
Description: Fetches and displays job listings from your Bullhorn account. To use, just add [wp_bullhorn] shortcode to any page or post.
Author: Ethan Allen
Author URI: http://endif.media
Version: 1.1
*/

/**
 *  plugin specific version of the_excerpt
 *	
 *	Set $word_limit to any number to limit posts to that number of words.
 *
 *	Use: 
 *	call function on string variable $content and set $word_limt to 35 -> <?php string_limit_words($content, 35); ?>
 */
function string_limit_words($string, $word_limit, $link) {
  $words = explode(' ', $string, ($word_limit + 1));
  if(count($words) > $word_limit);
  array_pop($words);
  return implode(' ', $words) . '... <a href="' . $link . '"><strong> Read More &rarr;</strong></a>';
}

/** 
 * Custom confirmation redirect
 * 
 */
function custom_confirmation($confirmation, $form, $lead, $ajax){
    if($form["id"] == "2"){
	$url = get_site_url() . '/thank-you/';
        $confirmation = array("redirect" =>"$url");
    }
    return $confirmation;
}
add_filter("gform_confirmation", "custom_confirmation", 10, 4);

/** 
 * Enqueue CSS ONLY on the settings page
 * 
 */
function bullhorn_admin_css($screen_page) {

 	//get current screen
 	$screen_page = get_current_screen();

 	//add plugin css ONLY to settings page
	if( $screen_page->id == 'toplevel_page_bullhorn_menu_page' ){
		wp_enqueue_style( 'wp-bullhorn-css', plugins_url( 'css/plugin-styles.css', __FILE__ ),'20140605', false );
	}

}
add_action( 'admin_enqueue_scripts', 'bullhorn_admin_css' );

/**
 * Install script
 *
 */
function pro_install() {
	add_option( 'bullhorn_username' );
	add_option( 'bullhorn_password' );
	add_option( 'bullhorn_api_key' );
	add_option( 'bullhorn_jobs', '10' );
	add_option( 'bullhorn_response_type', 'code' );
}
register_activation_hook(__FILE__,'pro_install');

/**
 * Uninstall script
 *
 */
function pro_uninstall() {
    global $wpdb;
	$table_name = $wpdb->prefix . "bullhorn_jobs";
    //$structure = "drop table if exists $table_name";
    $wpdb->query($structure);  

    delete_option( 'bullhorn_username' );
	delete_option( 'bullhorn_password' );
	delete_option( 'bullhorn_api_key' );
	delete_option( 'bullhorn_jobs' );
	delete_option( 'bullhorn_response_type' );
	delete_option( 'bullhorn_gravity_form' );

}
register_deactivation_hook(__FILE__ , 'pro_uninstall' );

/**
 *	Resgister Settings Page
 *
 */
function register_bullhorn_page(){
	add_menu_page( 'WP BULLHORN', 'WP BULLHORN', 'manage_options', 'bullhorn_menu_page', 'bullhorn_options_page', plugins_url( 'wp_bullhorn/icon.png' )); 
}
add_action( 'admin_menu', 'register_bullhorn_page' );

/**
 * Generate job listings and add shortcode functionality
 *	
 */
function WP_BULLHORN(){
	include("frontend/job-listing.php");
}
add_shortcode('wp_bullhorn','WP_BULLHORN');

/**
 * Generate Options Page
 *	
 */
function bullhorn_options_page(){

	//Add support for decimal numbers 
    if (!current_user_can('manage_options')) {
      wp_die( _e('You do not have sufficient permissions to access this page.', 'animated-login') );
    }

    // See if the user has posted us some information
    if( isset($_POST['submit']) ){

    	// Check for nonce
    	check_admin_referer( 'bullhorn_save_form', 'bullhorn_name_of_nonce' );
 		 
    	$api_key = sanitize_text_field( $_POST['api_key'] );
		$username = sanitize_text_field( $_POST['username'] );
		$password = sanitize_text_field( $_POST['password'] );
		$jobs = sanitize_text_field( $_POST['jobs'] );
		$g_form = $_POST['gravity-form'];

        // Check if inputs have a value
        if( !empty($api_key) || !empty($username) || !empty($password) ){       	

        	// update options 	
        	update_option( 'bullhorn_api_key', $api_key );
			update_option( 'bullhorn_username', $username ); 
			update_option( 'bullhorn_password', $password );
			update_option( 'bullhorn_jobs', $jobs );
			update_option( 'bullhorn_gravity_form', $g_form );

			$wp_bullhorn_success = true; //issue success variable 
	        
	    // Fail if options are empty
	    } else {
	      $wp_bullhorn_fail = 'Please enter a value for ALL fields.'; 
          // function is open until the end of the form
         // Output Message 
	  }
?>

<?php if(isset($wp_bullhorn_fail)) { ?>
<div class="error">
	<p><strong><?php _e("$wp_bullhorn_fail", 'wp-bullhorn' ); ?></strong></p>
</div>
<?php } ?>

<?php if(isset($wp_bullhorn_success) && $wp_bullhorn_success == true) { ?>
<div class="updated">
	<p><strong><?php _e('settings saved.', 'wp-bullhorn' ); ?></strong></p>
</div>
<?php } ?>

<?php }

    echo '<div class="wrap">';

    echo '<h2>' . __( 'WP BULLHORN - settings', 'wp-bullhorn' ) . '</h2><br>
          <p>' . __( 'Add your API key, username, and password. To display your job listings', 'wp-bullhorn') . '<br>'
          . __( 'simply add ', 'wp-bullhorn') . '<span class="shortcode-span">[wp_bullhorn]</span>' . 
          __( ' to any page or post.', 'wp-bullhorn') . '</p>';

?>
		<form id="wp-bullhorn-login-form" method="post" action="">
		  <?php wp_nonce_field( 'bullhorn_save_form', 'bullhorn_name_of_nonce' ); ?>
		  <table class="form-table">
		 	<tr valign="top">
				<th scope="row"><label>API Key <span class="red">*</span></label></th>
				<td>
					<input type="text" value="<?php print esc_attr( get_option('bullhorn_api_key') ); ?>" name="api_key" size="45" required>
				</td>
		    </tr>
			<tr valign="top">
				<th scope="row"><label>Username<span class="red">*</span></label></th>
				<td>
					<input type="text" value="<?php print esc_attr( get_option('bullhorn_username') ); ?>" name="username" size="45" required>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><label>Password<span class="red">*</span></label></th>
				<td>
					<input type="text" value="<?php print esc_attr( get_option('bullhorn_password') ); ?>" name="password" required>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><label>Number of Jobs to display<span class="red">*</span></label></th>
				<td>
					<input type="text" value="<?php print esc_attr( get_option('bullhorn_jobs') ); ?>" name="jobs" required>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><label>Response type</span></label></th>
				<td>
					<input type="text" value="code" name="response_type" required readonly>
				</td>
			</tr>
			<tr valign="middle">
			   <th scope="row">
				  <label for="gravity-form"><?php _e( 'Choose a signup form', 'wp-bullhorn' ); ?></label>
			   </th>
			   <td>
			   	  <select name="gravity-form" class="">
			   	  	
				   	<?php 

						$forms = RGFormsModel::get_forms( null, 'title' );

							foreach( $forms as $form ):

						 		 $select .= '<option value="' . esc_attr( $form->id ) . '" ' . selected( get_option( 'bullhorn_gravity_form' ), $form->id ) . '>' . $form->title . '</option>';

							endforeach;

						echo $select;
				    ?>
		  	       
				   </select>	
			   </td>
		    </tr>		  	 
		  </table>
		  <p class="submit">
		   <input type="submit" class="button-primary" name="submit" value="<?php _e('Save Changes') ?>" />
		  </p>
		</form>
	</div>
<?php } // function bullhorn_options_page() closed 